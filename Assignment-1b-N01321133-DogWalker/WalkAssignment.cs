﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1b_N01321133_DogWalker
{
    public class WalkAssignment
    {
        public Client cli;
        public Dog dg;
        public Order ord;

        public WalkAssignment(Client c, Dog d, Order o) {
            cli = c;
            dg = d;
            ord = o;
        }

        public object DogRes { get; private set; }

        public string ShowAssignmentDetails()
        {
            string s = "Client Name : " + cli.ClientFirstName + " " + cli.ClientLastName+"< br/>"
                + "Client Address:" + cli.ClientAddress + "<br/>"
                + "Client Email:" + cli.ClientEmail + "<br/>"
                + "Client Phone:" + cli.ClientPhone+"<br/>"
                + "Dog Name :" + dg.dName + "<br/>"
                + "Dog Breed :" + dg.dBreed + "<br/>"
                + "Grooming Required:" + dg.dGrooming+"<br/>"
                + "The dog walk time is: " + ord.time.ToString();
            return s;
        }
    }
}