﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1b_N01321133_DogWalker
{
    public class Client
    {
        private string clientFirstName;
        private string clientLastName;
        private string clientEmail;
        private string clientPhone;
        private string clientAddress;

        public Client()
        {

        }
        public string ClientFirstName
        {
            get { return clientFirstName; }
            set { clientFirstName = value; }
        }
        public string ClientLastName
        {
            get { return clientLastName; }
            set { clientLastName = value; }
        }
        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }
        public string ClientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }
        }
        public string ClientAddress
        {
            get { return clientAddress; }
            set { clientAddress = value; }
        }
    }
}