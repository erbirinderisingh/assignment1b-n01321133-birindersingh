﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Assignment_1b_N01321133_DogWalker.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <meta charset="utf-8"/>
    <title>Assignment1b-Dog Walker</title>
    <meta name="description" content="Assignment 1b, Section B, Birinder Singh, N01321133"/>
    <meta name="author" content="Birinder Singh, N01321133"/>
</head>
<body>
    <form id="form1" runat="server">
        <div id="registrationform" runat="server">
            <h2>Birinder's Dog Walking Services</h2>
            <!--Client Information -->
            <fieldset>
            <legend>Client's Detail</legend>
            <div class="row">
                <label for="clientFirstName">Client Last Name</label>
                <asp:Textbox runat="server" ID="clientFirstName" placeholder="First Name"></asp:Textbox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter first name" ID="clientFirstNameRequiredValidator" ControlToValidate="clientFirstName"></asp:RequiredFieldValidator>
            </div>
            <div class="row">
                <label for="clientLastName">Client Last Name</label>
                <asp:Textbox runat="server" ID="clientLastName" placeholder="Last Name"></asp:Textbox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter last name" ID="clientLastNameRequiredValidator" ControlToValidate="clientLastName"></asp:RequiredFieldValidator>
            </div>
            <div class="row">
                <label for="clientAddress">Client Address</label>
                <asp:Textbox runat="server" ID="clientAddress" placeholder="Address"></asp:Textbox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter address" ID="clientAddressRequiredValidator" ControlToValidate="clientAddress"></asp:RequiredFieldValidator>
            </div>
            <div class="row">
                <label for="clientEmail">Client EMail</label>
                <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="clientEmailRequiredValidator" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="regexEmailValid" ErrorMessage="Email wrong format" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail"></asp:RegularExpressionValidator>
            </div>
            <div class="row">
                <label for="clientPhone">Client Phone</label>
                <asp:Textbox runat="server" ID="clientPhone" placeholder="Phone"></asp:Textbox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter phone" ID="clientPhoneRequiredValidator" ControlToValidate="clientPhone"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="clientPhoneValidator" ErrorMessage="Phone in wrong format" ValidationExpression="^\d{10}$" ControlToValidate="clientPhone"></asp:RegularExpressionValidator>
            </div>
            </fieldset>

            <fieldset>
            <legend>Dog's Detail</legend>
                <div class="row">
                    <label for="dogName">Dog Name</label>
                    <asp:Textbox runat="server" ID="dogName" placeholder="First Name"></asp:Textbox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter dog name" ID="dogNameValidator" ControlToValidate="dogName"></asp:RequiredFieldValidator>
                </div>
                <label>Dog Breed</label>
                <asp:DropDownList runat="server" ID="dogBreed">
                    <asp:ListItem Value="Bull Dog" Text="Bull Dog"></asp:ListItem>
                    <asp:ListItem Value="Golden Retriver" Text="Golden Retriever"></asp:ListItem>
                    <asp:ListItem Value="Pug" Text="Pug"></asp:ListItem>
                </asp:DropDownList>
                <br/>
                <label>Grooming Required</label><br />
                <asp:RadioButtonList runat="server" ID="dogGrooming">
                    <asp:ListItem Text="Yes">Yes</asp:ListItem>
                    <asp:ListItem Text="No">No</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>

            <fieldset>
            <legend>Order Detail</legend>
                <label>Time</label>
                 <asp:DropDownList runat="server" ID="orderTime">
                    <asp:ListItem Value="1100" Text="1100hrs"></asp:ListItem>
                    <asp:ListItem Value="1200" Text="1200hrs"></asp:ListItem>
                    <asp:ListItem Value="1300" Text="1300hrs"></asp:ListItem>
                    <asp:ListItem Value="1400" Text="1400hrs"></asp:ListItem>
                    <asp:ListItem Value="1500" Text="1500hrs"></asp:ListItem>
                    <asp:ListItem Value="1600" Text="1600hrs"></asp:ListItem>
                    <asp:ListItem Value="1700" Text="1700hrs"></asp:ListItem>
                    <asp:ListItem Value="1800" Text="1800hrs"></asp:ListItem>
                    <asp:ListItem Value="1900" Text="1900hrs"></asp:ListItem>
                    <asp:ListItem Value="2000" Text="2000hrs"></asp:ListItem>
                </asp:DropDownList>
             </fieldset>
            <br />
                <asp:Button runat="server" ID="myButton" OnClick="NewOrder" Text="Submit"/>
            <br />

            <br />
            <div runat="server" id="FinalOrder">

            </div>
        </div>
    </form>
</body>
</html>
