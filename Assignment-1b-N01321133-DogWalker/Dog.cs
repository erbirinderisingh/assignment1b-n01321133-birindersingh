﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1b_N01321133_DogWalker
{
    public class Dog
    {
        public string dName;
        public string dBreed;
        public bool dGrooming;

        public Dog(string n, string b, bool g)
        {
            dName = n;
            dBreed = b;
            dGrooming = g;
        }
    }
}