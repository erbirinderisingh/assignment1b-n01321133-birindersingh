﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment_1b_N01321133_DogWalker
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void NewOrder(object sender, EventArgs e)
        {
            /*Client Object */
            string cfname = clientFirstName.Text.ToString();
            string clname = clientLastName.Text.ToString();
            string caddress = clientAddress.Text.ToString();
            string cemail = clientEmail.Text.ToString().ToLower();
            string cphone = clientPhone.Text.ToString();
            Client newclient = new Client();
            newclient.ClientFirstName = cfname;
            newclient.ClientLastName = clname;
            newclient.ClientPhone = cphone;
            newclient.ClientEmail = cemail;
            newclient.ClientAddress = caddress;

            /* Dog Object */
            string dname = dogName.Text.ToString();
            string dbreed = dogBreed.SelectedItem.Value.ToString();
            bool dGrooming;
            if (dogGrooming.SelectedValue == "Yes")
            {
                dGrooming = true;
            }
            else
            {
                dGrooming = false;
            }
            Dog newdog = new Dog(dname, dbreed, dGrooming);


            /* Order Object */
            int oTime = int.Parse(orderTime.Text);
            Order order = new Order(oTime);


            WalkAssignment w = new WalkAssignment(newclient,newdog,order);
            FinalOrder.InnerHtml=w.ShowAssignmentDetails();
        }
    }
}